from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from scenicspot.models import ScenicSpot


class UserProfile(models.Model):
    """
    用户信息表
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nickname = models.CharField(max_length=99, null=True, blank=True, default=None, help_text="昵称")
    phone = models.CharField(max_length=16, default="", null=True, blank=True, help_text="手机号")


class Viewing(models.Model):
    """
    用户资源观看记录表
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    scenic_spot = models.ForeignKey(ScenicSpot, on_delete=models.CASCADE, null=True, blank=True,
                                    related_name='scenic_spot')
    last_watch = models.DateTimeField(auto_now=True, help_text="最后观看时间")


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.userprofile.save()
