from datetime import timedelta

from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from .models import *


class UserSerializer(serializers.ModelSerializer):
    phone = serializers.CharField(source='userprofile.phone', help_text="用户号码", required=False,
                                  validators=[UniqueValidator(queryset=UserProfile.objects.all(), message='号码已存在')])
    username = serializers.CharField(required=False, help_text="用户名",
                                     validators=[UniqueValidator(queryset=User.objects.all(), message="用户名已存在")])
    password = serializers.CharField(help_text="密码", write_only=True, required=False, style={'input_type': 'password'},
                                     min_length=6, max_length=16)
    nickname = serializers.CharField(source='userprofile.nickname', help_text="昵称", required=False)

    class Meta:
        model = User
        fields = ['id', 'username', 'phone', 'is_staff', 'is_superuser', 'is_active',
                  'date_joined', 'password', 'nickname']