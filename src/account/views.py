import logging

from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken

from .serializers import *

logger = logging.getLogger(__name__)
collect_logger = logging.getLogger("collect")


class UserViewSet(viewsets.ModelViewSet):
    """
    create:创建用户
    identify: 查询用户信息
    list: 用户列表  is_active=true 活跃用户 false 是非活跃用户
    update: 修改用户信息
    retrieve: 管理员查询用户信息
    set_password: 修改密码
    set_active: 冻结或激活用户
    set_phone: 修改绑定号码
    destroy: 删除用户
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]
    search_fields = ['username', 'userprofile__phone', 'userprofile__nickname']

    # filter_backends = (filters.DjangoFilterBackend, SearchFilter, OrderingFilter)
    # filter_class = UserFilter

    @action(detail=False, methods=['GET'])
    def identify(self, request):
        instance = request.user

        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def perform_create(self, serializer):
        return serializer.save()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            errors = serializer.errors
            for i in errors:
                return Response({
                    'success': False,
                    'msg': errors[i][0]
                }, 400)

        userprofile = False
        if serializer.validated_data.get('userprofile', None):
            userprofile = serializer.validated_data.pop('userprofile')
        user = self.perform_create(serializer)

        if userprofile:
            UserProfile.objects.filter(user=user).update(**userprofile)

        refresh = RefreshToken.for_user(user)

        return Response({
            'success': True,
            'data': {
                'refresh': str(refresh),
                'access': str(refresh.access_token),
            },
            'msg': '成功'
        }, status=201)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)

        if self.request.user.is_staff:
            instance = self.get_object()
        else:
            instance = self.request.user

        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        userprofile_validated_data = serializer.validated_data.pop('userprofile')
        self.perform_create(serializer)

        if userprofile_validated_data:
            UserProfile.objects.filter(id=instance.userprofile.id).update(**userprofile_validated_data)
        user = User.objects.get(id=instance.id)
        data = self.serializer_class(user)
        return Response(data.data, 200)
