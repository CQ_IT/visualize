/**

 @Name：layui.blog 闲言轻博客模块
 @Author：徐志文
 @License：MIT
 @Site：http://www.layui.com/template/xianyan/
    
 */
layui.define(['element', 'form','laypage','jquery','laytpl'],function(exports){
  var element = layui.element
  ,form = layui.form
  ,laypage = layui.laypage
  ,$ = layui.jquery
  ,laytpl = layui.laytpl;
  

  //statr 分页
  
  laypage.render({
    elem: 'test1' //注意，这里的 test1 是 ID，不用加 # 号
    ,count: 50 //数据总数，从服务端得到
    ,theme: '#1e9fff'
  });
  
  // end 分頁
 


  // start 导航显示隐藏
  
  $("#mobile-nav").on('click', function(){
    $("#pop-nav").toggle();
  });

  // end 导航显示隐藏




  //start 评论的特效
  
  (function ($) {
    $.extend({
        tipsBox: function (options) {
          options = $.extend({
            obj: null,  //jq对象，要在那个html标签上显示
            str: "+1",  //字符串，要显示的内容;也可以传一段html，如: "<b style='font-family:Microsoft YaHei;'>+1</b>"
            startSize: "12px",  //动画开始的文字大小
            endSize: "30px",    //动画结束的文字大小
            interval: 600,  //动画时间间隔
            color: "red",    //文字颜色
            callback: function () { }    //回调函数
          }, options);

          $("body").append("<span class='num'>" + options.str + "</span>");

          var box = $(".num");
          var left = options.obj.offset().left + options.obj.width() / 2;
          var top = options.obj.offset().top - 10;
          box.css({
            "position": "absolute",
            "left": left + "px",
            "top": top + "px",
            "z-index": 9999,
            "font-size": options.startSize,
            "line-height": options.endSize,
            "color": options.color
          });
          box.animate({
            "font-size": options.endSize,
            "opacity": "0",
            "top": top - parseInt(options.endSize) + "px"
          }, options.interval, function () {
            box.remove();
            options.callback();
          });
        }
      });
  })($); 

  function niceIn(prop){
    prop.find('i').addClass('niceIn');
    setTimeout(function(){
      prop.find('i').removeClass('niceIn'); 
    },1000);    
  }

  $(function () {
    $(".like").on('click',function () {
     
      if(!($(this).hasClass("layblog-this"))){
        this.text = '已赞';
        $(this).addClass('layblog-this');
        $.tipsBox({
          obj: $(this),
          str: "+1",
          callback: function () {
          }
        });
        niceIn($(this));
        layer.msg('推荐成功', {
          icon: 6
          ,time: 1000
        })
      } 
    });
  });

  //end 评论的特效


  // start点赞图标变身
  $('#LAY-msg-box').on('click', '.info-img', function(){
    $(this).addClass('layblog-this');
  })


  // end点赞图标变身

  //end 提交
  $('#item-btn').on('click', function(){
    var elemCont = $('#LAY-msg-content')
    ,content = elemCont.val();
    if(content.replace(/\s/g, '') == ""){
      layer.msg('请先输入留言');
      return elemCont.focus();
    }

    var view = $('#LAY-msg-tpl').html()

    //模拟数据
    ,data = {
      username: '闲心'
      ,avatar: '../res/static/images/info-img.png'
      ,praise: 0
      ,content: content
    };

    //模板渲染
    laytpl(view).render(data, function(html){
      $('#LAY-msg-box').prepend(html);
      elemCont.val('');
      layer.msg('留言成功', {
        icon: 1
      })
    });

  })

  // start  图片遮罩
  var layerphotos = document.getElementsByClassName('layer-photos-demo');
  for(var i = 1;i <= layerphotos.length;i++){
    layer.photos({
      photos: ".layer-photos-demo"+i+""
      ,anim: 0
    }); 
  }
  // end 图片遮罩

  var defal = '·历经1400多年的西街，是阳朔最古老繁华的街道，也是阳朔重要旅游景点之一。\
  ·西街由觅食、逛街、泡吧等多个词汇集结而成，正因如此丰富多彩，成了阳朔一道独特而靓丽的风景线。\
  ·不足1km 的街道由石板砌成，呈弯曲的S形，两旁房屋古朴典雅，桂北明清时期风格，小青瓦、坡屋面、白粉墙、吊阳台。\
  ·街上摆满了各种旅游纪念品，两旁的商铺都是中西合璧的，几乎所有的招牌都是中英文对照，服务员能用流利外语为游客服务。\
  ·来西街，久负盛名当属阳朔第一名菜的啤酒鱼不可不尝，这里还有很多酒吧，夜幕降临，比白昼更加热闹。'
  var url = function(_url){
    var backend = 'http://127.0.0.1:8000';
    res = backend+_url;
    return res;
  }
  var url_change = function(url){
      let a = url.split('?')[1] || '';
      let b = a.split('&');
      let data = {};
      for(i in b){
        let c = b[i];
        let d = c.split('=');
        data[d[0]] = d[1];
      }

      return data;
    }

    // start 百度地图api
    
    var mapSetting = function(lng, lat){
        var map = new BMapGL.Map("adress");
        var point = new BMapGL.Point(lng, lat);
        let r = map.centerAndZoom(point, 19);
        map.enableScrollWheelZoom(true);
    }
    // end 百度地图api
  
    //  start vue数据绑定
    var app = new Vue({
      el: '#app',
      data: {
        scenics: [{'id': 1,'name': '阳朔西街', 'address': '广西壮族自治区桂林市阳朔县城中路西街', 'popularity': 0, 'reading': 0, 'details': defal, 'type': '', 'cover': 'https://n1-q.mafengwo.net/s11/M00/AA/72/wKgBEFt6YMOAaSGxABZLvE0A5r899.jpeg?imageMogr2%2Fthumbnail%2F%21690x450r%2Fgravity%2FCenter%2Fcrop%2F%21690x450%2Fquality%2F90%7Cwatermark%2F1%2Fimage%2FaHR0cHM6Ly9iMS1xLm1hZmVuZ3dvLm5ldC9zMTEvTTAwLzQ0LzlCL3dLZ0JFRnNQNVJ5QUR2N3BBQUFIWlpVUFJsUTk5MC5wbmc%3D%2Fgravity%2FSouthEast%2Fdx%2F10%2Fdy%2F11'}]
        ,scenic: {'id': 1,'name': '阳朔西街', 'address': '广西壮族自治区桂林市阳朔县城中路西街', 'popularity': 0, 'reading': 0, 'details': defal, 'type': '', 'cover': 'https://n1-q.mafengwo.net/s11/M00/AA/72/wKgBEFt6YMOAaSGxABZLvE0A5r899.jpeg?imageMogr2%2Fthumbnail%2F%21690x450r%2Fgravity%2FCenter%2Fcrop%2F%21690x450%2Fquality%2F90%7Cwatermark%2F1%2Fimage%2FaHR0cHM6Ly9iMS1xLm1hZmVuZ3dvLm5ldC9zMTEvTTAwLzQ0LzlCL3dLZ0JFRnNQNVJ5QUR2N3BBQUFIWlpVUFJsUTk5MC5wbmc%3D%2Fgravity%2FSouthEast%2Fdx%2F10%2Fdy%2F11'}
      },
      mounted: function(){
        let href = window.location.href;
        href = decodeURI(href);
        // this.getRecommend();

        if (href.indexOf('details') != -1){
            url_data = url_change(href);
            let id = url_data.id;
            this.getDetail(id);
        }

        if (href.indexOf('search') != -1){
            url_data = url_change(href);
            let title = url_data.title || '';
            if (title){
                this.getSearch(title);
            }
        }
        
      },
      methods: {
        getRecommend: function(){
            $.get(url('/api/scenicspot/scenicspots/recommend/'), {'action': 'getRecom'}, function(res){
                console.log(res);
                app.scenics = res;
            });
        },
        getDetail: function(id){
            
            $.get(url('/api/scenicspot/scenicspots/'+id+'/'), {}, function(res){
                app.scenic = res;
                app.changeMap(res.address);
                
            });
        },
        getSearch: function(title){
            $.ajax({
                url: url('/api/scenicspot/scenicspots/'),
                data: {'search': title, 'ordering': 'popularity'},
                dataType: 'json',
                type: 'get',
                success: function(data){
                    app.scenics = data;
                },
                error: function(e){
                    console.log('err', e);
                }
            });
        },
        changeMap: function(address){
            map_url = 'http://api.map.baidu.com/place/v2/search?query='+address+'&region=桂林&output=json&ak=mcAQBG2z1C7cuTfG3IOstGZEB2bj5wob';
            $.ajax({
                url: map_url,
                data: {},
                dataType: 'jsonp',
                type: 'get',
                success: function(res){
                    data = res.results;
                    mapSetting(data[0].location.lng, data[0].location.lat);
                }
            })
        }
      }

    });
    // end vue数据绑定

    
  
  //输出test接口
  exports('blog', {}); 
});  
