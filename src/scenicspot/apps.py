from django.apps import AppConfig


class ScenicspotConfig(AppConfig):
    name = 'scenicspot'
