import operator
from math import sqrt, pow

from django.contrib.auth.models import User

from .models import ScenicSpot
from random import randint, sample

class UserCf:

    def __init__(self, data):
        self.data = data

    def pearson(self, user1):
        sumXY = 0.0
        n = 0
        sumX = 0.0
        sumY = 0.0
        sumX2 = 0.0
        sumY2 = 0.0
        try:
            for item in ScenicSpot.objects.values('popularity', 'reading', 'id'):
                # 计算公共的的评分
                n += 1
                sumXY += 1
                sumX += float(item.get('popularity'))
                sumY += float(item.get('reading'))
                try:
                    sumX2 += pow(self.data.get(user=user1).scenic_spot.popularity, 2)
                    sumY2 += pow(self.data.get(user=user1).scenic_spot.reading, 2)
                except:
                    sumX2 += randint(0, 9)
                    sumY2 += randint(0, 9)

            molecule = sumXY - (sumX * sumY) / n
            denominator = sqrt((sumX2 - pow(sumX, 2) / n) * (sumY2 - pow(sumY, 2) / n))
            r = molecule / denominator
        except Exception as e:
            print("异常信息:", e)
            return None
        return r

    # 计算与当前用户的距离，获得最临近的用户
    def nearstUser(self, username, n=1):
        distances = {}  # 用户，相似度
        for items in User.objects.all():  # 遍历整个数据集
            if not items is username:  # 非当前的用户
                distance = self.pearson(items)  # 计算两个用户的相似度
                distances[items] = distance
        sortedDistance = sorted(distances.items(), key=operator.itemgetter(1), reverse=True)  # 最相似的N个用户
        print("排序后的用户为：", sortedDistance)
        return sortedDistance[:n]

    # 给用户推荐
    def recomand(self, username, n=1):
        recommand = []  # 待推荐的
        for user, score in dict(self.nearstUser(username, n)).items():  # 最相近的n个用户
            print("推荐的用户：", (user, score))
            for movies in self.data.filter(user=user):  # 推荐的用户的列表
                if movies not in self.data.exclude(user=username):  # 当前username没有看过
                    if movies not in recommand:  # 添加到推荐列表中
                        recommand.append(movies)

        return recommand  # 对推荐的结果按照评分排序
