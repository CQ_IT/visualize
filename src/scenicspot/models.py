import datetime
import os

from django.contrib.auth.models import User
from django.db import models


def pic_path(instance, pic_name):
    extension = os.path.splitext(pic_name)[1]
    now = datetime.datetime.now()
    pic = 'cover/' + now.strftime('%y%m%d_%H%M%S') + extension
    return pic


# Create your models here.
class ScenicSpot(models.Model):
    """
    景区
    """
    Type = (
        ('scenic', '景区'),
        ('hotel', '酒店'),
    )

    name = models.CharField(max_length=99, null=True, blank=True, default=None, help_text="景区名")
    address = models.CharField(max_length=99, null=True, blank=True, default=None, help_text="地址")
    popularity = models.PositiveIntegerField(default=0, help_text="人气")
    reading = models.PositiveIntegerField(default=0, help_text="观看量")
    details = models.TextField(null=True, blank=True, default=None, help_text="详情")
    type = models.CharField(choices=Type, max_length=99, null=True, blank=True, default=None, help_text="类型")
    cover = models.URLField(null=True, blank=True, default=None, help_text="封面图")

