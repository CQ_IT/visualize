import csv
import os

from django.conf import settings

from .models import ScenicSpot


def pull_data():
    try:
        files = os.path.join(settings.BASE_DIR, 'files')
        for i in os.listdir(files):
            data = csv.reader(open(os.path.join(files, i), 'r', encoding="UTF-8"))
            for idx, row in enumerate(data):
                if idx == 0 or not bool(row[0]):
                    continue
                ScenicSpot.objects.get_or_create(name=row[0],
                                                 cover=row[1],
                                                 details=row[2],
                                                 address=row[3],
                                                 popularity=row[4],
                                                 reading=row[5],
                                                 type='scenic' if row[6] in ["景点", "景区"] else 'hotel')
            os.unlink(os.path.join(files, i))
        return "更新完成"
    except:
        return "拉取失败"
