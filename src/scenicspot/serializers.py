from rest_framework import serializers

from .models import *
from account.serializers import UserSerializer


class ScenicSpotSerializer(serializers.ModelSerializer):

    class Meta:
        model = ScenicSpot
        fields = '__all__'
