from django.db.models import Q
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from account.models import Viewing
from .cf import *
from .serializers import *


class ScenicSpotViewSet(viewsets.ModelViewSet):
    queryset = ScenicSpot.objects.all()
    serializer_class = ScenicSpotSerializer
    search_fields = ['name', 'address', 'details']
    filter_fields = ['type']

    def retrieve(self, request, *args, **kwargs):
        from account.models import Viewing
        instance = self.get_object()
        # 观看量+1
        instance.reading += 1

        if request.user.is_active:
            new_datetime = datetime.datetime.today()

            if not Viewing.objects.filter(user=request.user, scenic_spot=instance, last_watch__day=new_datetime.day,
                                          last_watch__year=new_datetime.year, last_watch__month=new_datetime.month):
                Viewing.objects.create(user=request.user, scenic_spot=instance)
                # 人气+1
                instance.popularity += 1

        instance.save()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    @action(detail=False, methods=['GET'])
    def read_file(self, request, *args, **kwargs):
        """读取文件"""
        from .read_file import pull_data
        result = pull_data()
        return Response({
            'msg': result
        }, 200)

    @action(detail=False, methods=['GET'])
    def recommend(self, request, *args, **kwargs):
        """
        推荐列表
        """
        user = request.user
        user = user if user.is_active else None
        queryset = self.filter_queryset(self.get_queryset())
        try:
            userCf = UserCf(data=Viewing.objects.all())
            recommandList = userCf.recomand(user, 2)
            scenic_spot_id = [i.scenic_spot_id for i in recommandList] if recommandList else None
            scenic_spot_id = scenic_spot_id if scenic_spot_id else [i[0] for i in
                                                                    sample(list(queryset.values_list('id')), 20)]
            queryset = self.get_queryset().filter(id__in=scenic_spot_id)
        except Exception as e:
            raise e
        user = user if user and user.is_active else User.objects.get(id=1)
        scenic_spot = [i[0] for i in Viewing.objects.filter(user=user).values_list('scenic_spot_id')]
        if user and user.is_active:
            queryset1 = queryset.order_by('-popularity').order_by('-scenic_spot')
        else:
            queryset1 = queryset.order_by('-scenic_spot')

        queryset = self.filter_queryset(self.get_queryset().filter(Q(scenic_spot__in=scenic_spot)) | queryset1)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset.distinct()[:10], many=True)
        # if not queryset.distinct():
        #     print("aaaaa")
        return Response(serializer.data)
